import {
  html,
  component,
  useState,
  useEffect
} from "https://unpkg.com/haunted/haunted.js";

function VideoPlayer(el) {
  // const [first, setFirst] = useState("Happy");
  // const [last, setLast] = useState("Halloween 🎃");

  // useEffect(() => {
  //   const event = new CustomEvent('change', {
  //     detail: `${first} ${last}`
  //   });
  //   this.dispatchEvent(event);
  // }, [first, last]);

  return html`
    <link type="text/css" rel="stylesheet" href="node_modules/video.js/dist/video-js.min.css" />
    <div class="video-wrapper">
      <video
        id="my-video"
        class="video-js"
        controls
        preload="auto"
        width="640"
        height="264"
        poster="http://localhost/tiny-castle.jpg"
        data-setup="{}"
      >
        <source src="http://localhost/station.mp4" type="video/mp4" />
        <p class="vjs-no-js">
          To view this video please enable JavaScript, and consider upgrading to a
          web browser that
          <a href="https://videojs.com/html5-video-support/" target="_blank"
            >supports HTML5 video</a
          >
        </p>
      </video>
      <br>
      <video
        id="u-tube-vid"
        class="video-js vjs-default-skin"
        controls
        autoplay
        width="640" height="264"
        data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "https://www.youtube.com/watch?v=LQUXuQ6Zd9w"}], "youtube": { "ytControls": 2 } }'
      >
      </video>
    </div>
  <script src="../node_modules/video.js/dist/video.js"></script>
  <script src="../node_modules/videojs-youtube/dist/Youtube.js"></script>
  `;
}

customElements.define("video-player", component(VideoPlayer));
