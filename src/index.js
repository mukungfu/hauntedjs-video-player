import {
  html,
  component,
  useState,
} from "https://unpkg.com/haunted/haunted.js";
import "./VideoPlayer.js";

function App() {
  const [name, setName] = useState("");

  return html`
    <fieldset>
      <legend>HTML5 VideoPlayer Widget:</legend>
      <video-player></video-player>
    </fieldset>

    <style>
      fieldset {
        border: none;
      }

      legend {
        padding: 0;
        margin-bottom: 0.5rem;
      }
    </style>
  `;
}

customElements.define("my-app", component(App));
